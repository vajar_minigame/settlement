package handler

import (
	"net"

	log "github.com/sirupsen/logrus"
	"gitlab.com/vajar_minigame/proto_go/gen/settlement"
	"gitlab.com/vajar_minigame/settlement/model"
	"gitlab.com/vajar_minigame/settlement/repository"
	"gitlab.com/vajar_minigame/settlement/serviceloop"
	"google.golang.org/grpc"
)

const port = "localhost:8086"

func StartServer() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("error listening on port: %s with err: %s", port, err)
	}
	db := repository.NewSqliteDB()
	loop := serviceloop.ServiceLoop{DB: db, BuildingDef: model.InitDefContainer()}
	go loop.BuildingQueueLoop()
	go loop.ResourceProductionLoop()
	s := grpc.NewServer()
	settlement.RegisterSettlementServicesServer(s, NewConstructionHandler(db))
	log.Infof("start server on port %s", port)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
