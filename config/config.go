package config

type Config struct {
	DBConnectionString string
	SettlementSize     int
}
