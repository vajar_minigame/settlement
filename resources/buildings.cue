buildingDefs: Defs: {
	Lumberjack: {
		BuildCosts: {
			"WOOD": 20
		}
		Produces: [{
			ProducedResource: "WOOD"
			ProducedAmount:   1
			TimeToBuild:      "2s"
		}]}
	MainBuilding: {
		BuildCosts: {
			"WOOD": 10
		}
		Produces: []

	}
	Sawmill: {
		BuildCosts: {
			"WOOD": 10
		}
		Produces: [
			{
				RequiresAmount: {
					"WOOD": 1
				}
				ProducedResource: "PLANK"
				ProducedAmount:   1
				TimeToBuild:      "2s"
			},
		]
	}
}
