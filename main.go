package main

import "gitlab.com/vajar_minigame/settlement/handler"

func main() {
	handler.StartServer()
}
