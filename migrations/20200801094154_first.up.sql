CREATE Table settlement (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    pos_x INTEGER NOT NULL,
    pos_y INTEGER NOT NULL
);
CREATE TABLE depot (
    resource_id INTEGER NOT NULL,
    settlement_id INTEGER NOT NULL,
    capacity INTEGER NOT NULL,
    current_stored INTEGER NOT NULL,
    last_production DATETIME NOT NULL,
    PRIMARY KEY (resource_id, settlement_id),
    FOREIGN KEY(settlement_id) REFERENCES settlement(id)
);
CREATE TABLE placed_buildings (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    settlement_id INTEGER NOT NULL,
    build_def TEXT NOT NULL,
    in_construction INTEGER,
    hibernated INTEGER,
    last_produced DATETIME NOT NULL,
    FOREIGN KEY (settlement_id) REFERENCES settlement (id)
);
CREATE TABLE build_queue (
    settlement_id INTEGER,
    building_id INTEGER PRIMARY KEY,
    build_start DATETIME NOT NULL,
    build_end DATETIME NOT NULL,
    FOREIGN KEY(settlement_id) REFERENCES settlement(id),
    FOREIGN KEY(building_id) REFERENCES placed_buildings(id)
);