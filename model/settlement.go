package model

import (
	"fmt"
	"time"

	"github.com/golang/protobuf/ptypes"
	log "github.com/sirupsen/logrus"
	"gitlab.com/vajar_minigame/proto_go/gen/resources"
	"gitlab.com/vajar_minigame/proto_go/gen/settlement"
)

type Settlement struct {
	ID             int64 `db:"id"`
	Pos            Position
	Buildings      []Building
	BuildQueueSize int64
	BuildQueue     []BuildQueueItem
	Depot          DepotStore
}
type DepotStore struct {
	Store          map[resources.ResourceType]ResourceStorage
	LastProduction time.Time
}

type BuildQueueItem struct {
	StartTime  time.Time
	EndTime    time.Time
	BuildingID int64
}

type Position struct {
	X int64 `db:"pos_x"`
	Y int64 `db:"pos_y"`

	//later
	sizeX int64
	sizeY int64
}

type ResourceStorage struct {
	Capacity      int64
	CurrentStored int64
}

func SettlementFromProto(settle *settlement.Settlement) (*Settlement, error) {
	buildings := make([]Building, 0)
	for _, v := range settle.Buildings {
		buildings = append(buildings, Building{ID: int64(v.Id), BuildingDefID: v.BuildDef, IsInConstruction: v.InConstruction})
	}

	depot := DepotStore{Store: make(map[resources.ResourceType]ResourceStorage)}

	for k, v := range settle.Depot.Store {

		depot.Store[resources.ResourceType(k)] = ResourceStorage{Capacity: v.Capacity, CurrentStored: v.CurrentStored}
	}

	pos := Position{X: settle.Pos.X, Y: settle.Pos.Y}

	buildQueue := make([]BuildQueueItem, 0)
	for _, v := range settle.BuildQueue {
		startTime, err := ptypes.Timestamp(v.BuildStart)
		if err != nil {
			return nil, fmt.Errorf("could not convert proto timestamp err: %w", err)
		}
		endTime, err := ptypes.Timestamp(v.BuildEnd)
		if err != nil {
			return nil, fmt.Errorf("could not convert proto timestamp err: %w", err)
		}
		buildQueue = append(buildQueue, BuildQueueItem{EndTime: endTime, BuildingID: v.BuildingId, StartTime: startTime})
	}

	return &Settlement{ID: settle.Id, Buildings: buildings, Depot: depot, Pos: pos, BuildQueue: buildQueue, BuildQueueSize: settle.BuildQueueSize}, nil

}

func (s *Settlement) ToProto() *settlement.Settlement {

	buildings := make([]*settlement.Building, 0)
	for _, v := range s.Buildings {
		buildings = append(buildings, &settlement.Building{Id: v.ID, BuildDef: string(v.BuildingDefID), InConstruction: v.IsInConstruction})
	}
	depot := &settlement.Depot{Store: make([]*settlement.ResourceStorage, len(resources.ResourceType_value))}
	for k, v := range s.Depot.Store {
		depot.Store[k] = &settlement.ResourceStorage{Capacity: v.Capacity, CurrentStored: v.CurrentStored}
	}

	buildQueue := make([]*settlement.BuildQueueItem, 0)
	for _, v := range s.BuildQueue {
		startTime, err := ptypes.TimestampProto(v.StartTime)
		if err != nil {
			log.Errorf("could not convert proto timestamp err: %s", err)
		}
		endTime, err := ptypes.TimestampProto(v.EndTime)
		if err != nil {
			log.Errorf("could not convert proto timestamp err: %s", err)
		}
		buildQueue = append(buildQueue, &settlement.BuildQueueItem{BuildingId: v.BuildingID, BuildStart: startTime, BuildEnd: endTime})
	}

	return &settlement.Settlement{Id: s.ID, Buildings: buildings, BuildQueue: buildQueue, Depot: depot, Pos: &settlement.Position{X: s.Pos.X, Y: s.Pos.Y}}

}
