package model

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/vajar_minigame/proto_go/gen/resources"
)

type BuildingDefinition struct {
	BuildingDefID string
	//cost per level
	BuildCosts map[resources.ResourceType]int64
	Produces   []Production
	Tags       []string

	Effects     []IncreaseEffect
	TimeToBuild time.Duration
}

type InputBuildingDefinition struct {
	//cost per level
	BuildCosts  map[string]int64
	Produces    []InputProduction
	TimeToBuild Duration
}

type InputProduction struct {
	RequiresAmount   map[string]int64
	ProducedResource string
	ProducedAmount   int64
	TimeToBuild      Duration
}

type Production struct {
	RequiresAmount        map[resources.ResourceType]int64
	ProducedResource      resources.ResourceType
	ProducedAmount        int64
	RequiresUpgradeFactor float64
	ProduceUpgradeFactor  float64
	TimeToBuild           time.Duration
}

type InputBuildingDefinitionsContainer struct {
	Defs map[string]InputBuildingDefinition
}

//more depot?

type IncreaseEffect struct {
	DepotValIncrease    map[resources.ResourceType]ResourceStorage
	UpgradeMultipFactor float64
}

func (c *InputBuildingDefinitionsContainer) ToBuildingsContainer() (BuildingDefinitionsContainer, error) {
	defs := make(map[string]BuildingDefinition)

	for key, d := range c.Defs {

		buildCosts := make(map[resources.ResourceType]int64)

		for k, cost := range d.BuildCosts {
			res, ok := resources.ResourceType_value[k]
			if ok == false {
				return BuildingDefinitionsContainer{
					Defs: defs,
				}, fmt.Errorf("resource3 %s is undefined", k)
			}
			buildCosts[resources.ResourceType(res)] = cost
		}

		production := make([]Production, 0)

		for _, p := range d.Produces {
			requiresAmount := make(map[resources.ResourceType]int64)
			for k, amount := range p.RequiresAmount {
				res, ok := resources.ResourceType_value[k]
				if ok == false {
					return BuildingDefinitionsContainer{
						Defs: defs,
					}, fmt.Errorf("resource2 %s is undefined", k)
				}
				requiresAmount[resources.ResourceType(res)] = amount
			}

			//producedResource
			res, ok := resources.ResourceType_value[p.ProducedResource]
			if ok == false {
				return BuildingDefinitionsContainer{
					Defs: defs,
				}, fmt.Errorf("resource1 %s is undefined", key)
			}

			production = append(production, Production{RequiresAmount: requiresAmount, ProducedResource: resources.ResourceType(res), ProducedAmount: p.ProducedAmount, TimeToBuild: time.Duration(p.TimeToBuild)})

		}

		defs[key] = BuildingDefinition{BuildCosts: buildCosts, Produces: production, TimeToBuild: time.Duration(d.TimeToBuild), BuildingDefID: key}
	}
	return BuildingDefinitionsContainer{
		Defs: defs,
	}, nil
}

type BuildingDefinitionsContainer struct {
	Defs map[string]BuildingDefinition
}

func InitDefContainer() BuildingDefinitionsContainer {

	jsonFile, err := os.Open("resources/buildings.json")
	if err != nil {
		logrus.Fatalf("could not load building definitions err: %s", err)
	}

	buildingsDefinitions := make(map[string]InputBuildingDefinitionsContainer, 0)

	file, _ := ioutil.ReadAll(jsonFile)
	err = json.Unmarshal(file, &buildingsDefinitions)
	if err != nil {
		logrus.Fatalf("could not unmarshal building definitions err: %s", err)
	}

	fmt.Printf("%+v", buildingsDefinitions)
	test, _ := buildingsDefinitions["buildingDefs"]
	container, err := test.ToBuildingsContainer()
	if err != nil {
		logrus.Fatalf("could not unmarshal building definitions err: %s", err)
	}
	return container

}
