package model

import "time"

type Building struct {
	ID               int64     `db:"id"`
	IsInConstruction bool      `db:"in_construction"`
	Hibernated       bool      `db:"hibernated"`
	BuildingDefID    string    `db:"build_def"`
	LastProduced     time.Time `db:"last_update"`
	Level            int       `db:"level"`
}

func NewBuilding(ID int64, buildingDefID string, isInConstruction bool) Building {
	return Building{ID: ID, Level: 1, IsInConstruction: isInConstruction, BuildingDefID: buildingDefID}
}
