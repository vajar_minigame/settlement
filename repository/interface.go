package repository

import (
	"context"
	"errors"

	"gitlab.com/vajar_minigame/proto_go/gen/common"
	"gitlab.com/vajar_minigame/settlement/model"
)

type SettlementDB interface {
	AddSettlement(context.Context, *model.Settlement) (*model.Settlement, error)
	GetSettlementByID(context.Context, *common.SettlementId) (*model.Settlement, error)
	GetSettlementsByUserID(context.Context, *common.UserId) ([]*model.Settlement, error)
	DeleteSettlement(context.Context, *common.SettlementId) error
	AddBuildingToQueue(context.Context, *common.SettlementId, model.BuildQueueItem) error
	RemoveBuildingFromQueue(context.Context, *common.BuildingId) error
	GetConstructedBuildings(context.Context) ([]*model.BuildQueueItem, error)

	AddBuilding(context.Context, *common.SettlementId, model.Building) (*model.Building, error)
	GetBuilding(context.Context, *common.BuildingId) (*model.Building, error)
	UpdateBuilding(context.Context, *model.Building) error
	UpdateDepo(context.Context, *model.Settlement) error

	//escape hatches
	GetAllSettlements(context.Context) (SettlementIterator, error)
}

var IteratorEnd = errors.New("no more values")

type SettlementIterator interface {
	Next() (*model.Settlement, error)
}
