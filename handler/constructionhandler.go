package handler

import (
	"context"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/vajar_minigame/minigame_backend/config"
	"gitlab.com/vajar_minigame/proto_go/gen/common"
	"gitlab.com/vajar_minigame/proto_go/gen/resources"
	"gitlab.com/vajar_minigame/proto_go/gen/settlement"
	"gitlab.com/vajar_minigame/settlement/model"
	"gitlab.com/vajar_minigame/settlement/repository"
)

type constructionHandler struct {
	settlement.UnimplementedSettlementServicesServer
	config       config.Config
	db           repository.SettlementDB
	defContainer model.BuildingDefinitionsContainer
}

func NewConstructionHandler(db repository.SettlementDB) *constructionHandler {

	return &constructionHandler{db: db, defContainer: model.InitDefContainer()}

}

func (h *constructionHandler) AddSettlement(ctx context.Context, settlement *settlement.Settlement) (*settlement.Settlement, error) {
	s, _ := model.SettlementFromProto(settlement)

	s.Buildings = []model.Building{{BuildingDefID: "MainBuilding", IsInConstruction: false}}
	s.Depot = model.DepotStore{Store: make(map[resources.ResourceType]model.ResourceStorage), LastProduction: time.Now()}
	s.Depot.Store[resources.ResourceType_WOOD] = model.ResourceStorage{Capacity: 100, CurrentStored: 30}
	s.Depot.Store[resources.ResourceType_PLANK] = model.ResourceStorage{Capacity: 100, CurrentStored: 0}

	res, err := h.db.AddSettlement(ctx, s)
	if err != nil {
		wErr := fmt.Errorf("could not add settlement err:%w", err)
		log.Error(wErr)
		return nil, wErr
	}
	return res.ToProto(), nil
}
func (h *constructionHandler) AddTestSettlement(context.Context, *settlement.Settlement) (*settlement.Settlement, error) {
	return nil, nil
}
func (h *constructionHandler) GetSettlementByID(ctx context.Context, settlementID *common.SettlementId) (*settlement.Settlement, error) {
	s, err := h.db.GetSettlementByID(ctx, settlementID)
	if err != nil {
		return nil, fmt.Errorf("could not get settlement err:%w", err)
	}
	return s.ToProto(), nil
}
func (h *constructionHandler) GetSettlementsByUserID(context.Context, *common.UserId) (*settlement.SettlementList, error) {
	return nil, nil
}
func (h *constructionHandler) DeleteSettlement(context.Context, *common.SettlementId) (*common.ResponseStatus, error) {
	return nil, nil
}

func (h *constructionHandler) BuildBuilding(ctx context.Context, call *settlement.BuildBuildingCall) (*common.ResponseStatus, error) {
	//check if buildingDef exists
	def, ok := h.defContainer.Defs[call.BuildDef]
	if ok == false {
		err := fmt.Errorf("could not find definition %s", call.BuildDef)
		log.Error(err)
		return &common.ResponseStatus{Success: false}, err
	}
	s, err := h.db.GetSettlementByID(ctx, call.SettlementId)
	if err != nil {
		err := fmt.Errorf("could not find settlement %d err %w", call.SettlementId.Id, err)
		log.Error(err)
		return &common.ResponseStatus{Success: false}, err
	}
	//check if building is allowed
	for _, b := range s.Buildings {
		if string(b.BuildingDefID) == call.BuildDef {
			err := fmt.Errorf("building %s already exists", call.BuildDef)
			log.Warn(err)
			return &common.ResponseStatus{Success: false}, err
		}
	}

	// check resources
	for m, c := range def.BuildCosts {
		stored, ok := s.Depot.Store[m]
		if ok == false || stored.CurrentStored < c {
			err := fmt.Errorf("not enough resources of type %s", m)
			log.Warn(err)
			return &common.ResponseStatus{Success: false}, err
		}
		newVal := s.Depot.Store[m]
		newVal.CurrentStored -= c
		s.Depot.Store[m] = newVal

	}
	h.db.UpdateDepo(ctx, s)
	// add building; add to buildQueue

	building := model.NewBuilding(-1, call.BuildDef, true)
	b, err := h.db.AddBuilding(ctx, call.SettlementId, building)
	if err != nil {
		err := fmt.Errorf("could not add Building %w", err)
		log.Error(err)
		return &common.ResponseStatus{Success: false}, err
	}
	duration := time.Second * 5
	bqItem := model.BuildQueueItem{StartTime: time.Now(), BuildingID: b.ID, EndTime: time.Now().Add(duration)}
	err = h.db.AddBuildingToQueue(ctx, call.SettlementId, bqItem)
	if err != nil {
		err := fmt.Errorf("could not add Building to buildQueue %w", err)
		log.Error(err)
		return &common.ResponseStatus{Success: false}, err
	}
	return &common.ResponseStatus{Success: true}, nil
}
