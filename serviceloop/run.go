package serviceloop

import (
	"context"
	"errors"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/vajar_minigame/proto_go/gen/common"
	"gitlab.com/vajar_minigame/settlement/model"
	"gitlab.com/vajar_minigame/settlement/repository"
)

type ServiceLoop struct {
	DB          repository.SettlementDB
	BuildingDef model.BuildingDefinitionsContainer
}

func (s *ServiceLoop) BuildingQueueLoop() {

	for range time.Tick(time.Second * 5) {
		ctx := context.Background()
		constructedBuildings, err := s.DB.GetConstructedBuildings(ctx)
		if err != nil {
			logrus.Fatalf("getting constructed buildings err:%v", err)
		}

		for _, bI := range constructedBuildings {
			building, err := s.DB.GetBuilding(ctx, &common.BuildingId{Id: bI.BuildingID})

			if err != nil {
				logrus.Fatalf("get constructed buildings err:%v", err)
			}
			logrus.Infof("building %s was built sucessfully", building.ID)

			building.IsInConstruction = false
			building.LastProduced = time.Now()
			err = s.DB.UpdateBuilding(ctx, building)
			if err != nil {
				logrus.Fatalf("updating constructed buildings err:%v", err)
			}

			err = s.DB.RemoveBuildingFromQueue(ctx, &common.BuildingId{Id: bI.BuildingID})
			if err != nil {
				logrus.Fatalf("removing item from queue err:%v", err)
			}
		}

	}

}

//ResourceProductionLoop iterate over all buildings of each settlement and add produced resources to depot
func (s *ServiceLoop) ResourceProductionLoop() {
	ctx := context.Background()
	for range time.Tick(time.Second * 5) {
		iter, err := s.DB.GetAllSettlements(ctx)
		if err != nil {
			logrus.Fatalf("getting constructed buildings err:%v", err)
		}

		for {
			smt, err := iter.Next()
			if errors.Is(err, repository.IteratorEnd) {
				break
			} else if err != nil {
				logrus.Fatalf("getting settlements failed err:%v", err)
			}

			for i, b := range smt.Buildings {
				def := s.BuildingDef.Defs[b.BuildingDefID]
				if b.IsInConstruction {
					continue
				}
				for _, p := range def.Produces {
					delta := time.Now().Sub(b.LastProduced)

					for k, count := range p.RequiresAmount {
						decrement := int64(delta/p.TimeToBuild) * count
						if smt.Depot.Store[k].CurrentStored-decrement < 0 {
							decrement = smt.Depot.Store[k].CurrentStored
							//guaranteed to be smaller than last delta
							delta = time.Duration(decrement/count) * p.TimeToBuild
						}

					}

					//could shrink when no warez to consume are available or storage if nearly full
					increment := int64(delta/p.TimeToBuild) * p.ProducedAmount

					newStore := smt.Depot.Store[p.ProducedResource]
					if newStore.CurrentStored+increment > newStore.Capacity {
						increment = newStore.Capacity - smt.Depot.Store[p.ProducedResource].CurrentStored

					}
					delta = time.Duration(increment/p.ProducedAmount) * p.TimeToBuild

					// execute with found delta that works for everything
					for k, count := range p.RequiresAmount {
						decrement := int64(delta/p.TimeToBuild) * count
						newStore := smt.Depot.Store[k]
						newStore.CurrentStored -= decrement
						smt.Depot.Store[k] = newStore

					}

					//could shrink when no warez to consume are available or storage if nearly full
					newStore = smt.Depot.Store[p.ProducedResource]
					newStore.CurrentStored += increment
					smt.Depot.Store[p.ProducedResource] = newStore
					if delta < 100 {
						b.LastProduced = time.Now()
					} else {
						b.LastProduced = b.LastProduced.Add(delta)
					}

					smt.Buildings[i] = b
					s.DB.UpdateBuilding(ctx, &b)

				}
				smt.Depot.LastProduction = time.Now()
				s.DB.UpdateDepo(ctx, smt)

			}

		}
	}
}
