param ($command, $subCommand, $name)
$ConnectionString = "sqlite3:///project/data.db"


function build() {
    echo "test"
}

function testfunktion()
{ echo "Hello Word!" } 

function migrate() {
    switch ($subCommand) {
        "up" { migrate_up }
        "down" { migrate_down }
        "create" { migrate_create }
        "drop" { migrate_drop }
    }
}
function migrate_drop() {
    docker run -v $PWD/:/project --network host migrate/migrate -path=/project/migrations -database="$ConnectionString" drop -f
}
function migrate_down() {
    docker run -v $PWD/:/project --network host migrate/migrate -path=/project/migrations -database="$ConnectionString" down --all
}
function migrate_up() {
    docker run -v $PWD/:/project --network host migrate/migrate -path=/project/migrations -database="$ConnectionString" up
}

function migrate_create() {
    docker run -v $PWD/:/project --network host migrate/migrate create -ext sql -dir /project/migrations $name 
}


switch ($command) {
    "build" { build }
    "migrate" { migrate }
    
}






