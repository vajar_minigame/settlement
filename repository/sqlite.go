package repository

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/vajar_minigame/proto_go/gen/common"
	"gitlab.com/vajar_minigame/proto_go/gen/resources"
	"gitlab.com/vajar_minigame/settlement/model"
)

type sqliteDB struct {
	db *sqlx.DB
}

func NewSqliteDB() SettlementDB {
	db, err := sqlx.Open("sqlite3", "file:data.db")
	if err != nil {
		log.Fatal("error opening db", "error", err)
	}

	err = db.Ping()
	if err != nil {
		log.Fatal("error opening db", "error", err)
	}

	return &sqliteDB{db: db}
}

func (db *sqliteDB) AddSettlement(ctx context.Context, settlement *model.Settlement) (*model.Settlement, error) {
	resultSettlement := settlement
	const insertStatement = `INSERT INTO settlement( pos_x,pos_y) VALUES(?,?)`

	res, err := db.db.Exec(insertStatement, settlement.Pos.X, settlement.Pos.Y)
	if err != nil {
		return nil, fmt.Errorf("error adding settlement  err:%w", err)
	}
	settlementID, err := res.LastInsertId()
	resultSettlement.ID = settlementID
	if err != nil {
		return nil, fmt.Errorf("error adding settlement  err:%w", err)
	}

	if err := (db.UpdateDepo(ctx, resultSettlement)); err != nil {
		return nil, err
	}
	err = db.BulkInsertBuilding(ctx, settlement)
	if err != nil {
		return nil, fmt.Errorf("error inserting buildings \t%w", err)
	}
	return resultSettlement, err

}
func (db *sqliteDB) GetSettlementByID(ctx context.Context, settlementID *common.SettlementId) (*model.Settlement, error) {
	var s = &model.Settlement{}
	const getQuery = `SELECT id,pos_x,pos_y FROM settlement WHERE id=?`
	row := db.db.QueryRow(getQuery, settlementID.Id)
	if row == nil {
		return nil, nil
	}

	const getQueryDepot = `SELECT resource_id,capacity,current_stored,last_production FROM depot WHERE settlement_id=?`

	err := row.Scan(&s.ID, &s.Pos.X, &s.Pos.Y)
	if err != nil {
		return nil, fmt.Errorf("error query sqlite db: %w", err)
	}

	rows, err := db.db.Query(getQueryDepot, settlementID.Id)
	if err != nil {
		return nil, fmt.Errorf("error unmarshaling depot: %w", err)
	}
	depo := model.DepotStore{Store: make(map[resources.ResourceType]model.ResourceStorage)}
	s.Depot = depo
	for rows.Next() {
		var resourceID resources.ResourceType
		resStorage := model.ResourceStorage{}
		err := rows.Scan(&resourceID, &resStorage.Capacity, &resStorage.CurrentStored, &s.Depot.LastProduction)
		if err != nil {
			return nil, fmt.Errorf("error unmarshaling depot: %w", err)
		}
		s.Depot.Store[resourceID] = resStorage

	}
	const getQueryBuilding = `SELECT id,build_def,in_construction,last_produced FROM placed_buildings WHERE settlement_id=?`
	rows, err = db.db.Query(getQueryBuilding, settlementID.Id)
	if err != nil {
		return nil, fmt.Errorf("error unmarshaling building: %w", err)
	}
	s.Buildings = make([]model.Building, 0)
	for rows.Next() {
		b := model.Building{}
		err := rows.Scan(&b.ID, &b.BuildingDefID, &b.IsInConstruction, &b.LastProduced)
		if err != nil {
			return nil, fmt.Errorf("error unmarshaling depot: %w", err)
		}
		s.Buildings = append(s.Buildings, b)
	}

	const getQueryBuildQueue = `SELECT building_id,build_start,build_end FROM build_queue WHERE settlement_id=? ORDER BY build_start`
	s.BuildQueue = make([]model.BuildQueueItem, 0)
	rows, err = db.db.Query(getQueryBuildQueue, settlementID.Id)
	if err != nil {
		return nil, fmt.Errorf("error unmarshaling building: %w", err)
	}
	for rows.Next() {
		b := model.BuildQueueItem{}
		err := rows.Scan(&b.BuildingID, &b.StartTime, &b.EndTime)
		if err != nil {
			return nil, fmt.Errorf("error unmarshaling buildQueue: %w", err)
		}
		s.BuildQueue = append(s.BuildQueue, b)

	}

	return s, nil
}

func (db *sqliteDB) BulkInsertBuilding(ctx context.Context, settlement *model.Settlement) error {
	insertBuildingparams := []interface{}{}
	queryInsertBuilding := `INSERT INTO "placed_buildings" (settlement_id,build_def,in_construction,last_produced) VALUES `
	for _, res := range settlement.Buildings {
		queryInsertBuilding += `(?,?,?,?),`
		insertBuildingparams = append(insertBuildingparams, settlement.ID, res.BuildingDefID, res.IsInConstruction, res.LastProduced)
	}
	queryInsertBuilding = queryInsertBuilding[:len(queryInsertBuilding)-1]

	_, err := db.db.Exec(queryInsertBuilding, insertBuildingparams...)
	return err
}

func (db *sqliteDB) AddBuildingToQueue(ctx context.Context, sID *common.SettlementId, b model.BuildQueueItem) error {
	const insertInQueue = `INSERT INTO build_queue(building_id,settlement_id,build_start,build_end) VALUES(?,?,?,?)`
	_, err := db.db.Exec(insertInQueue, b.BuildingID, sID.Id, b.StartTime, b.EndTime)
	if err != nil {
		return fmt.Errorf("error adding settlement  err:%w", err)
	}
	return nil
}

func (db *sqliteDB) AddBuilding(ctx context.Context, sID *common.SettlementId, b model.Building) (*model.Building, error) {
	const insertInQueue = `INSERT INTO placed_buildings(settlement_id,build_def,in_construction,last_produced) VALUES(?,?,?,?)`
	res, err := db.db.Exec(insertInQueue, sID.Id, b.BuildingDefID, b.IsInConstruction, b.LastProduced)
	if err != nil {
		return nil, fmt.Errorf("error adding building  err:%w", err)
	}

	id, err := res.LastInsertId()
	if err != nil {
		return nil, fmt.Errorf("could net get inserted building id  err:%w", err)
	}

	resultB := b
	resultB.ID = id

	return &resultB, nil
}

func (db *sqliteDB) GetSettlementsByUserID(ctxc context.Context, userID *common.UserId) ([]*model.Settlement, error) {
	return nil, nil
}
func (db *sqliteDB) DeleteSettlement(ctx context.Context, settlementID *common.SettlementId) error {
	return nil
}

func (db *sqliteDB) GetConstructedBuildings(ctx context.Context) ([]*model.BuildQueueItem, error) {
	t := time.Now()
	const getQueryBuildQueue = `SELECT building_id,build_start,build_end FROM build_queue WHERE build_end<?`
	buildQueue := make([]*model.BuildQueueItem, 0)
	rows, err := db.db.Query(getQueryBuildQueue, t)
	if err != nil {
		return nil, fmt.Errorf("error unmarshaling building: %w", err)
	}
	for rows.Next() {
		b := model.BuildQueueItem{}
		err := rows.Scan(&b.BuildingID, &b.StartTime, &b.EndTime)
		if err != nil {
			return nil, fmt.Errorf("error unmarshaling buildQueue: %w", err)
		}
		buildQueue = append(buildQueue, &b)

	}
	return buildQueue, nil
}

func (db *sqliteDB) GetBuilding(ctx context.Context, bID *common.BuildingId) (*model.Building, error) {
	const getQueryBuilding = `SELECT id,build_def,in_construction FROM placed_buildings WHERE id=?`
	b := &model.Building{}
	err := db.db.Get(b, getQueryBuilding, bID.Id)
	if err != nil {
		return nil, fmt.Errorf("getting building db: %w", err)
	}
	return b, nil
}

func (db *sqliteDB) UpdateBuilding(ctx context.Context, b *model.Building) error {
	const updateBuilding = `UPDATE placed_buildings 
							  SET build_def=?,in_construction=?, hibernated=?,last_produced=?
							  WHERE id=?`
	_, err := db.db.Exec(updateBuilding, b.BuildingDefID, b.IsInConstruction, b.Hibernated, b.LastProduced, b.ID)
	if err != nil {
		return fmt.Errorf("updating building db: %w", err)
	}
	return nil
}

func (db *sqliteDB) RemoveBuildingFromQueue(ctx context.Context, bID *common.BuildingId) error {

	const deleteQueue = `DELETE FROM build_queue WHERE building_id=?`
	_, err := db.db.Exec(deleteQueue, bID.Id)
	if err != nil {
		return fmt.Errorf("error adding building  err:%w", err)
	}

	return nil
}

func (db *sqliteDB) UpdateDepo(ctx context.Context, s *model.Settlement) error {
	//delete everything then add new stuff
	const deleteQueue = `DELETE FROM depot WHERE settlement_id=?`
	_, err := db.db.Exec(deleteQueue, s.ID)
	if err != nil {
		return fmt.Errorf("error adding building  err:%w", err)
	}

	insertparams := []interface{}{}
	queryInsert := `INSERT INTO "depot" (resource_id,settlement_id,capacity,current_stored,last_production) VALUES `
	for key, res := range s.Depot.Store {
		queryInsert += `(?,?,?,?,?),`
		insertparams = append(insertparams, key, s.ID, res.Capacity, res.CurrentStored, s.Depot.LastProduction)
	}
	queryInsert = queryInsert[:len(queryInsert)-1]

	_, err = db.db.Exec(queryInsert, insertparams...)
	if err != nil {
		return fmt.Errorf("error inserting depot \t%w", err)
	}
	return nil

}

type settlementIter struct {
	db        *sqliteDB
	ids       []*common.SettlementId
	currIndex int32
}

func (i *settlementIter) Next() (*model.Settlement, error) {
	if i.currIndex >= int32(len(i.ids)) {
		return nil, IteratorEnd
	}

	//todo better solution
	s, err := i.db.GetSettlementByID(context.Background(), i.ids[i.currIndex])
	if err != nil {
		return nil, err
	}
	i.currIndex++

	return s, nil

}

func (db *sqliteDB) GetAllSettlements(context.Context) (SettlementIterator, error) {
	//reduce concurrency problems

	const getQuerySettlements = `SELECT id FROM settlement`
	rows, err := db.db.Queryx(getQuerySettlements)
	if err != nil {
		return nil, fmt.Errorf("error unmarshaling depot: %w", err)
	}
	ids := make([]*common.SettlementId, 0)
	for rows.Next() {
		var id int32
		_ = rows.Scan(&id)
		ids = append(ids, &common.SettlementId{Id: int64(id)})

	}
	return &settlementIter{ids: ids, db: db}, nil

}
