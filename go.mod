module gitlab.com/vajar_minigame/settlement

go 1.14

require (
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/golang/protobuf v1.4.2
	github.com/google/go-cmp v0.5.1 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.3.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.2
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.5.1 // indirect
	gitlab.com/vajar_minigame/minigame_backend v0.0.0-20200613100544-38b6b1942f5a
	gitlab.com/vajar_minigame/proto_go v0.0.0-20200818150523-b325292e36b6
	golang.org/x/net v0.0.0-20200822124328-c89045814202 // indirect
	golang.org/x/sys v0.0.0-20200828161417-c663848e9a16 // indirect
	google.golang.org/genproto v0.0.0-20200828030656-73b5761be4c5 // indirect
	google.golang.org/grpc v1.31.1
)
